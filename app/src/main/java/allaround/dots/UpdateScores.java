package allaround.dots;

public interface UpdateScores {
    void setViews( int score , int moves);
    void endGame(int score, int boardSize);
    void startTimer();
}