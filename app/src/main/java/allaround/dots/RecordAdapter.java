package allaround.dots;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class RecordAdapter extends ArrayAdapter<Record> {

    private final Context context;
    private final List<Record> values;
    private boolean isSize7 = false;
    private boolean isTimeGame = false;

    public RecordAdapter(Context context, List<Record> objects) {
        super(context, -1, objects);
        this.context = context;
        this.values = objects;
    }

    @Override
    public View getView( int position, View convertView, ViewGroup parent ) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.row_layout, parent,false);


        TextView nameView = (TextView) rowView.findViewById(R.id.row_name);
        TextView scoreView = (TextView) rowView.findViewById(R.id.row_score);

        if (!isSize7 && !isTimeGame)
            {
                if ( (values.get(position).getBoardSize() == 6) && !(values.get(position).isTimeGame()))
                {
                    nameView.setText( values.get(position).getName() );
                    scoreView.setText(String.valueOf(values.get(position).getScore()));
                }
            }
            else if (!isSize7 && isTimeGame)
            {
                if ( (values.get(position).getBoardSize() == 6) && (values.get(position).isTimeGame()))
                {
                    nameView.setText( values.get(position).getName() );
                    scoreView.setText(String.valueOf(values.get(position).getScore()));
                }
            }
            else if (isSize7 && !isTimeGame)
            {
                if ( (values.get(position).getBoardSize() == 7) && !(values.get(position).isTimeGame()))
                {
                    nameView.setText( values.get(position).getName() );
                    scoreView.setText(String.valueOf(values.get(position).getScore()));
                }
            }
            else if (isSize7 && isTimeGame)
            {
                if ( (values.get(position).getBoardSize() == 7) && (values.get(position).isTimeGame()))
                {
                    nameView.setText( values.get(position).getName() );
                    scoreView.setText(String.valueOf(values.get(position).getScore()));
                }
            }
//        nameView.setText( values.get(position).getName() );
//        scoreView.setText( String.valueOf(values.get(position).getScore()) );

        return rowView;
    }

//    @Override
//    public void notifyDataSetInvalidated()
//    {
//        int boardSize;
//        if (isSize7)
//            boardSize = 7;
//        else boardSize = 6;
//
//        for (int i = values.size() - 1; i >= 0; --i)
//        {
//            Record rec = values.get(i);
//
//        }
//
//
//
//        if(enableSections)
//        {
//            for (int i = items.size() - 1; i >= 0; i--)
//            {
//                Manga element = items.get(i);
//                String firstChar = element.getName().substring(0, 1).toUpperCase();
//                if(firstChar.charAt(0) > 'Z' || firstChar.charAt(0) < 'A')
//                    firstChar = "@";
//                alphaIndexer.put(firstChar, i);
//            }
//
//            Set<String> keys = alphaIndexer.keySet();
//            Iterator<String> it = keys.iterator();
//            ArrayList<String> keyList = new ArrayList<String>();
//            while (it.hasNext())
//            {
//                keyList.add(it.next());
//            }
//
//            Collections.sort(keyList);
//            sections = new String[keyList.size()];
//            keyList.toArray(sections);
//
//            super.notifyDataSetInvalidated();
//        }
//    }

    public void setSize7(boolean val)
    {
        isSize7 = val;
    }
    public void setTimeGame(boolean val)
    {
        isTimeGame = val;
    }

}