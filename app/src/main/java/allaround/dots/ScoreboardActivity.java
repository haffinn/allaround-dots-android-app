package allaround.dots;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import android.content.Context;
import android.view.View;

import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.ToggleButton;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;


public class ScoreboardActivity extends AppCompatActivity {

    private ListView m_listView;

    private ToggleButton m_gameType;
    private ToggleButton m_boardSize;
    private boolean isTimeGame;
    private boolean isSize7;

    Context context = this;

    ArrayList<Record> m_data = new ArrayList<Record>();
//    ArrayList<Record> m_t6 = new ArrayList<>();
//    ArrayList<Record> m_t7 = new ArrayList<>();
//    ArrayList<Record> t_t6 = new ArrayList<>();
//    ArrayList<Record> t_t7 = new ArrayList<>();
    RecordAdapter m_adapter;

//    RecordAdapter m_admt6;
//    RecordAdapter m_admt7;
//    RecordAdapter m_adtt6;
//    RecordAdapter m_adtt7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scoreboard);

        m_listView = (ListView) findViewById(R.id.records);
        m_adapter = new RecordAdapter(this, m_data);
        m_listView.setAdapter(m_adapter);



        m_gameType = (ToggleButton) findViewById(R.id.scoreboard_gameType);
        m_gameType.setTextOn("Move challenge");
        m_gameType.setTextOff("Time challenge");
        m_gameType.setChecked(true);
        isTimeGame = false;


        m_gameType.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                {
                    isTimeGame = false;
                    m_adapter.setTimeGame(false);
                    m_adapter.notifyDataSetChanged();
                }
                else
                {
                    isTimeGame = true;
                    m_adapter.setTimeGame(true);
                    m_adapter.notifyDataSetChanged();
                }
            }
        });


        m_boardSize = (ToggleButton) findViewById(R.id.scoreboard_boardsize);
        m_boardSize.setTextOn("6");
        m_boardSize.setTextOff("7");
        m_boardSize.setChecked(true);
        isSize7 = false;

        m_boardSize.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                {
                    isSize7 = false;
                    m_adapter.setSize7(false);
                    m_adapter.notifyDataSetChanged();
                }
                else
                {
                    isSize7 = true;
                    m_adapter.setSize7(true);
                    m_adapter.notifyDataSetChanged();
                }
            }
        });


        m_data.add(new Record("Siggi", 69, 6, true));
        m_data.add(new Record("Haffi", 1337, 6, false) );

        m_data.add(new Record("Yngvi", 15, 7, true));
        m_data.add(new Record("Hrabbi", 3, 7, false));
        m_data.add(new Record("Gaur", 20, 7, false));
        m_data.add(new Record("Gaur2", 21, 7, true));

        m_adapter.notifyDataSetChanged();
    }

    @Override
    public void onStart() {
        super.onStart();
        readRecords();

        Intent intent = getIntent();
        if (intent != null) {
            Bundle extras = intent.getExtras();
            if (extras != null) {
                Record rec = null;
                Record rec2 = null;
                try
                {
                    rec = (Record) extras.getSerializable("Record");
                }
                finally {
                    if (rec != null)
                        m_data.add(rec);
                }

                try
                {
                    rec2 = (Record) extras.getSerializable("Record2");
                }
                finally
                {
                    if (rec2 != null)
                        m_data.add(rec2);
                }

            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        writeRecords();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

//    public void storeRecord( Record record ) {
//        // String name = m_nameView.getText().toString();
//        if (record != null ) {
//            m_data.add(record);
//           //  m_nameView.setText("");
//            m_adapter.notifyDataSetChanged();
//        }
//    }

    void writeRecords( ) {
        try {
            //FileOutputStream fos = new FileOutputStream( "records.ser" );
            FileOutputStream fos = openFileOutput( "records.ser", Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(m_data);
            oos.close();
            fos.close();
        }
        catch ( IOException e ) {
            e.printStackTrace();
        }
    }

    void readRecords() {
        try {
            FileInputStream fis = openFileInput("records.ser");
            ObjectInputStream ois = new ObjectInputStream(fis);
            ArrayList<Record> records = (ArrayList) ois.readObject();
            ois.close();
            fis.close();


            m_data.clear();
            for ( Record rec: records ) {
                if(isSize7)
                {
                    if(rec.getBoardSize() == 7)
                    {
                        m_data.add( rec );
                    }
                }
                else if (!isSize7)
                {
                    if (rec.getBoardSize() == 6)
                    {
                        m_data.add (rec);
                    }
                }
            }
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
    }

    public void clearData() {
        m_data.clear();
        m_adapter.notifyDataSetChanged();
    }

    public void menuScoreboard(View view) {
    }

    public void clearScore(View view) {

        AlertDialog.Builder clearHighScore = new AlertDialog.Builder(context);

        clearHighScore
                .setMessage("Are you sure you want to clear all the high scores?")
                .setCancelable(true)
                .setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                clearData();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

        AlertDialog alertDialog = clearHighScore.create();
        alertDialog.show();
    }
}


        /*Button button = (Button) findViewById(R.id.ClearHighScore);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                clearData();
            }
        });*/