package allaround.dots;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static allaround.dots.Dot.*;
import static android.app.Activity.*;

public class BoardView extends View {

    private int m_cellWidth;
    private int m_cellHeight;

    private int m_score;
    private int m_moves;
    private int m_timer;

    private UpdateScores m_updateScoreHandler = null;
    private UpdateTimer m_updateTimerHandler = null;

    private boolean isTimeGame;


//    public Boolean isMove = false;

//    private Rect m_rect = new Rect();
//    private Paint m_paint = new Paint();

   //  private RectF m_circle = new RectF();
    // private Paint m_paintCircle = new Paint();

    private Path m_path = new Path();
    private Paint m_paintPath = new Paint();

    private ArrayList<ArrayList<Dot>> m_grid = new ArrayList<ArrayList<Dot>>();

    private MediaPlayer m_sound, m_swoosh, m_jeij;
    private Boolean m_use_sound = true;
    private Boolean m_use_swoosh = true;
    private Boolean m_use_jeij = true;
    private Vibrator m_vibrator;
    private Boolean m_use_vibrator = false;
    SharedPreferences m_sp;
    private int m_boardSize = 6;

    private int NUM_CELLS = m_boardSize;

    public BoardView(Context context, AttributeSet attrs) {
        super(context, attrs);
//        m_paint.setColor(Color.BLACK);
//        m_paint.setStyle(Paint.Style.STROKE);
//        m_paint.setStrokeWidth(2);
//        m_paint.setAntiAlias(true);

        m_paintPath.setColor(Color.BLACK);
        m_paintPath.setStrokeWidth(30.0f);
        m_paintPath.setStrokeJoin(Paint.Join.ROUND);
        m_paintPath.setStrokeCap(Paint.Cap.ROUND);
        m_paintPath.setStyle(Paint.Style.STROKE);
        m_paintPath.setAntiAlias(true);


        m_score = 0;
        m_moves = 30;
        m_timer = 30;

        m_sound = MediaPlayer.create(getContext(), R.raw.sound);
        m_swoosh = MediaPlayer.create(getContext(), R.raw.swoosh);
        m_jeij = MediaPlayer.create(getContext(), R.raw.jeij);
        m_vibrator = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);
        m_sp = PreferenceManager.getDefaultSharedPreferences(getContext());
        m_use_sound = m_sp.getBoolean("sound", true);
        m_use_swoosh = m_sp.getBoolean("sound", true);
        m_use_jeij = m_sp.getBoolean("sound", true);
        m_use_vibrator = m_sp.getBoolean("vibrate", false);
        m_boardSize = Integer.parseInt(m_sp.getString("board", "6"));
        NUM_CELLS = m_boardSize;

        String callFromActivity = context.getClass().getSimpleName();

        if (callFromActivity.contains("PlayTimeActivity")) {
            isTimeGame = true;
        } else {
            isTimeGame = false;
        }

        for(int i = 0; i < NUM_CELLS; i++){
            ArrayList<Dot> m_row = new ArrayList<Dot>();
            ArrayList<Paint> m_pc = new ArrayList<Paint>();
            for(int j = 0; j < NUM_CELLS; j++) {
                int myColor = getRandomColor();
                Dot dot = new Dot(i, j, myColor);
                m_row.add(dot);
            }
            m_grid.add(m_row);
        }
    }

    @Override
    protected void onMeasure( int widthMeasureSpec, int heightMeasureSpec ) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = getMeasuredWidth() - getPaddingLeft() - getPaddingRight();
        int height = getMeasuredHeight() - getPaddingTop() - getPaddingBottom();
        int size = Math.min(width, height);
        setMeasuredDimension(size + getPaddingLeft() + getPaddingRight(),
                size + getPaddingTop() + getPaddingBottom());
    }

    @Override
    protected void onSizeChanged( int xNew, int yNew, int xOld, int yOld ) {

        int   boardWidth = (xNew - getPaddingLeft() - getPaddingRight());
        int   boardHeight = (yNew - getPaddingTop() - getPaddingBottom());
        m_cellWidth = boardWidth / NUM_CELLS;
        m_cellHeight = boardHeight / NUM_CELLS;
        //m_rect.set(0, 0, boardWidth, boardHeight );
        //m_rect.offset( getPaddingLeft(), getPaddingTop());
    }

    @Override
    protected void onDraw(Canvas canvas ) {

//        ++rate;
//        String val = String.valueOf(rate);
//         Log.i("RATE:", val);


        for ( int row = 0; row < NUM_CELLS; ++row ) {
            ArrayList<Dot> current = m_grid.get(row);

            for (int col = 0; col < NUM_CELLS; ++col) {
                Dot dot = current.get(col);

                int x = col * m_cellWidth;
                int y = row * m_cellHeight;
//                m_rect.set(x, y, x + m_cellWidth, y + m_cellHeight);
//                m_rect.offset(getPaddingLeft(), getPaddingTop());
//                canvas.drawRect(m_rect, m_paint);

                dot.set(x, y, x + m_cellWidth, y + m_cellHeight);
                dot.inset(m_cellWidth * 0.2f, m_cellHeight * 0.2f);
                dot.offset(getPaddingLeft(), getPaddingTop());
                canvas.drawOval(dot, dot.getPaint());
            }
        }

        if ( !m_cellPath.isEmpty() ) {
            m_path.reset();
            Point point = m_cellPath.get(0);
            m_path.moveTo( colToX(point.x) + m_cellWidth/2, rowToY(point.y) + m_cellHeight/2 );
            for ( int i=1; i<m_cellPath.size(); ++i ) {
                point = m_cellPath.get(i);
                // m_path.moveTo( point.x, point.y);
                m_path.lineTo(colToX(point.x) + m_cellWidth / 2, rowToY(point.y) + m_cellHeight / 2);
            }
            canvas.drawPath( m_path, m_paintPath );
        }
    }

    boolean m_moving = false;

    private int xToCol( int x ) {
        return (x - getPaddingLeft()) / m_cellWidth;
    }
    private int yToRow( int y ) {
        return (y - getPaddingTop()) / m_cellHeight;
    }
    private int colToX( int col ) {
        return  col * m_cellWidth + getPaddingLeft();
    }
    private int rowToY( int row ) {
        return  row * m_cellHeight + getPaddingTop();
    }

    private boolean isAligned(Dot dotFrom, Dot dotTo)
    {

        int myRow = dotFrom.getRow();
        int myCol = dotFrom.getCol();

        int hisRow = dotTo.getRow();
        int hisCol = dotTo.getCol();

        if ((myRow == hisRow) && (hisCol == (myCol + 1)))
        {
            // check to the right
            return true;
        }
        else if ((myRow == hisRow) && (hisCol == (myCol - 1)))
        {
            // check to the left
            return true;
        }
        else if ((hisRow == (myRow + 1)) && (myCol == hisCol))
        {
            // check below
            return true;
        }
        else if ((hisRow == (myRow - 1)) && (myCol == hisCol))
        {
            // check above
            return true;
        }

        return false;
    }

    void snapToGrid( RectF circle ) {
        int col = xToCol((int) circle.left);
        int row = yToRow((int) circle.top);
        float x = colToX(col) + (m_cellWidth - circle.width())/2.0f;
        float y = rowToY(row) + (m_cellHeight - circle.height())/2.0f;
        circle.offsetTo( x, y );
    }

    private boolean isBacking (int myRow, int myCol)
    {
        if(m_dotPath.size() > 1) {
            Dot nextLastDot = m_dotPath.get(m_dotPath.size() - 2);

            if (nextLastDot.getRow() == myRow && nextLastDot.getCol() == myCol)
            {
                return true;
            }
        }
        return false;

    }

    private List<Point> m_cellPath = new ArrayList<Point>();
    private List<Dot> m_dotPath = new ArrayList<Dot>();

    @Override
    public boolean onTouchEvent( MotionEvent event ) {

//        Log.i("cellPath", m_cellPath.toString());
//        Log.i("dotPath", m_dotPath.toString());
        int x = (int) event.getX();
        int y = (int) event.getY();

        int xMax = getPaddingLeft() + m_cellWidth * NUM_CELLS;
        int yMax = getPaddingTop() + m_cellHeight * NUM_CELLS;

        Dot dot = new Dot();
        Boolean brk = false;
        for (int row = 0; row < NUM_CELLS; ++row)
        {
            if (brk) break;
            ArrayList<Dot> current = m_grid.get(row);
            for (int col = 0; col < NUM_CELLS; ++col)
            {
                Dot currDot = current.get(col);
                if (currDot.contains(x, y)) {
                        dot = currDot;
                        brk = true;
                        break;
                }
            }
        }

//        x = Math.max(getPaddingLeft(), Math.min(x, (int) (xMax - dot.width())));
//        y = Math.max(getPaddingTop(), Math.min(y, (int) (yMax - dot.height())));


        x = Math.max(getPaddingLeft(), Math.min(x, xMax));
        y = Math.max(getPaddingTop(), Math.min(y, yMax));

        if ( event.getAction() == MotionEvent.ACTION_DOWN ) {

            if ( dot.contains(x,y) ) {

                if(m_use_vibrator) {
                    m_vibrator.vibrate(50);
                    //Toast.makeText(getContext(), "Vibrate...", Toast.LENGTH_SHORT).show();
                }

                m_moving = true;
                m_cellPath.add(new Point(xToCol(x), yToRow(y)));
                m_dotPath.add(dot);
                int clr = dot.getColor();
                m_paintPath.setColor(clr);
            }
            invalidate();
        }
        else if ( event.getAction() == MotionEvent.ACTION_MOVE ) {
            if ( m_moving ) {
                Dot lastDot = m_dotPath.get(m_dotPath.size() - 1);
                if ( !m_cellPath.isEmpty() && (lastDot.getColor() == dot.getColor()) && isAligned(lastDot, dot) ) {
                    int col = xToCol(x);
                    int row = yToRow(y);
                    Point last = m_cellPath.get(m_cellPath.size() - 1);

                    if ((col != last.x || row != last.y)) {
                        if (isBacking(row, col))
                        {
                            m_cellPath.remove(m_cellPath.size() - 1);
                            m_dotPath.remove(m_dotPath.size() - 1);
                            playSound();
                            vibrate();
                        } else if (!m_dotPath.contains(dot)){
                            m_cellPath.add(new Point(col, row));
                            m_dotPath.add(dot);

                            if (isTimeGame)
                            {
                                startTimer();
                            }
                            playSound();
                            vibrate();
                        }
                    }
                }
                invalidate();
            }
        }
        else if ( event.getAction() == MotionEvent.ACTION_UP ) {
            m_moving = false;

            if (m_dotPath.size() > 1) {
                // Update score and moves/time
                m_score += m_dotPath.size();
                m_moves--;
                playSwoosh();
                m_updateScoreHandler.setViews(m_moves ,m_score);

                if (m_moves == 0 && !isTimeGame) {
                    endGame();
                }

                // Update dots in the board
                updateBoard();
            }

            m_cellPath.clear();
            m_dotPath.clear();
            invalidate();
        }
        return true;
    }

    public void setUpdateScoresHandler(UpdateScores update){
        m_updateScoreHandler = update;
    }
    public void setUpdateTimerHandler(UpdateTimer update) { m_updateTimerHandler = update; }

    public void updateBoard()
    {
        // For each column:
        // * Get all columns to update
        // * remove all rows (dots in col)
        // * "drop" all above dots down, if any
        // * Fill rest with random

        int col, row;
        ArrayList<Integer> columnsToUpdate = new ArrayList<>();
        HashMap<Integer, Integer> lowestRowInCol = new HashMap<>();
        HashMap<Integer, Integer> highestRowInCol = new HashMap<>();

        // Get all columns in DotPath, storing columns to update, and the upper and lower most dot in each row
        for( Dot dot : m_dotPath)
        {
            col = dot.getCol();
            row = dot.getRow();
            if (!columnsToUpdate.contains(col))
            {
                columnsToUpdate.add(col);
            }
            if ((!lowestRowInCol.containsKey(col)) || lowestRowInCol.get(col) > row)
            {
                lowestRowInCol.put(col, row);
            }
            if ((!highestRowInCol.containsKey(col)) || highestRowInCol.get(col) < row)
            {
                highestRowInCol.put(col, row);
            }
        }

        for (int myCol : columnsToUpdate)
        {
            ArrayList<Dot> dotsInCol = getDotsInCol(myCol);
            int lowestInRow = lowestRowInCol.get(myCol);
            int shift = highestRowInCol.get(myCol) - lowestInRow + 1;

            ArrayList<Dot> tempDotsInCol = new ArrayList<Dot>(dotsInCol);
            Collections.reverse(tempDotsInCol);

            for (Dot dot : tempDotsInCol)
            {
                if (dot.getRow() < lowestInRow)
                {
                    // Move dot down
                    shiftDown(dot, shift);
                }
            }
            addNewRandomDots(myCol, shift);
        }
        columnsToUpdate.clear();
        lowestRowInCol.clear();
        highestRowInCol.clear();
    }

    public void shiftDown(Dot dot, int squares)
    {
        Dot changeMe = m_grid.get(dot.getRow() + squares).get(dot.getCol());
        changeMe.setColor(dot.getColor());
    }

    public void addNewRandomDots(int col, int squares)
    {
        for (int i = 0; i < squares; ++i)
        {
            m_grid.get(i).get(col).setColor(getRandomColor());
        }
    }

    public ArrayList<Dot> getDotsInCol (int index)
    {
        ArrayList<Dot> myDots = new ArrayList<>();
        for (int row = 0; row < NUM_CELLS; ++row)
        {
            ArrayList<Dot> DotsInRow = m_grid.get(row);
            for (int col = 0; col < NUM_CELLS; ++col)
            {
                Dot dot = DotsInRow.get(col);
                if (dot.getCol() == index)
                {
                    myDots.add(dot);
                }
            }
        }

        return myDots;
    }


    public void playSound()
    {
        if (!m_use_sound) {
            m_sound.stop();
        } else {
            m_sound.start();
        }
    }

    public void playSwoosh()
    {
        if (!m_use_swoosh) {
            m_swoosh.stop();
        } else {
            m_swoosh.start();
        }
    }

    public void playJeij()
    {
        if (!m_use_jeij) {
            m_jeij.stop();
        } else {
            m_jeij.start();
        }
    }

    public void vibrate()
    {
        if (m_use_vibrator) {
            m_vibrator.vibrate(50);
            //Toast.makeText(getContext(), "Vibrate...", Toast.LENGTH_SHORT).show();
        }
    }

    public void startTimer()
    {
        m_updateScoreHandler.startTimer();
    }

    public void endGame()
    {
//        ((PlayGameActivity) getContext()).endGame(m_score);

        // Delay dialog for 0.5s and play sound
        new CountDownTimer(500, 1000) {
            public void onTick(long millisUntilFinished) {}
            public void onFinish() {
                playJeij();
                m_updateScoreHandler.endGame(m_score, NUM_CELLS);
            }
        }.start();

    }
}