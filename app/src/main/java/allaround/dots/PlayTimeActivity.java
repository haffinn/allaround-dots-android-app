package allaround.dots;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

public class PlayTimeActivity extends AppCompatActivity {

    public TextView updateScore;
    public TextView updateTime;
    BoardView m_boardViewTime;
    Context context = this;
    final boolean isTimeGame = true;
    private boolean counterStarted = false;
    CountDownTimer counter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_time);

        m_boardViewTime = (BoardView) findViewById(R.id.boardViewTime);
        updateScore = (TextView) findViewById(R.id.myScore);
        updateTime = (TextView) findViewById(R.id.myTime);

        updateScore.setText("0");
        updateTime.setText("30 s");

        counter = new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                updateTime.setText(String.valueOf(millisUntilFinished / 1000) + " s");
            }

            public void onFinish() {
                updateTime.setText("0");
                m_boardViewTime.endGame();
            }
        };


        m_boardViewTime.setUpdateScoresHandler(new UpdateScores() {
            @Override
            public void setViews(int moves, int score) {
                String scoreUpdater = String.valueOf(score);
                updateScore.setText(scoreUpdater);
            }

            @Override
            public void endGame(int score, int boardSize) {
                saveScore(score, boardSize);
            }

            @Override
            public void startTimer() {
                startClock();
            }
        });

        m_boardViewTime.setUpdateTimerHandler(new UpdateTimer() {
            @Override
            public void setTime(int time) {
                String timerUpdater = String.valueOf(time);
                updateTime.setText(timerUpdater);
            }
        });

    }

    public void startClock()
    {
        if (!counterStarted)
        {
            counter.start();
            counterStarted = true;
        }
    }

    public boolean getIsTime()
    {
        return this.isTimeGame;
    }

    private void saveScore(final int score, final int boardSize) {
        final EditText name = new EditText(context);

        name.setInputType(InputType.TYPE_CLASS_TEXT);
        AlertDialog.Builder gameOver = new AlertDialog.Builder(context);

        gameOver
                .setMessage("Score: " + score + "\nEnter your name")
                .setCancelable(false)
                .setView(name)
                .setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String playerName = name.getText().toString();
                                Record rec2 = new Record(playerName, score, boardSize, true);

                                Intent intent = new Intent(context, ScoreboardActivity.class);

                                //Senda record með
                                intent.putExtra("Record2", rec2);

                                startActivity(intent);
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                                Intent intent = new Intent(context, MainActivity.class);
                                startActivity(intent);
                            }
                        });

        AlertDialog alertDialog = gameOver.create();
        alertDialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_play_game, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}