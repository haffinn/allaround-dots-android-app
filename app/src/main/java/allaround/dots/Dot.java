package allaround.dots;

import android.graphics.Paint;
import android.graphics.RectF;

import java.util.Random;

public class Dot extends RectF {

    // private int mColor;
    private int mRow;
    private int mCol;

    private Paint mPaint;

    private float mLeft, mTop, mRight, mBottom;

                                 /* Purple,       Red,       Green,      Blue,       Orange   */
    private static final int[] colors = { 0xff9933cc, 0xffff0000, 0xff99cc00, 0xff33b5e5, 0xffff8800 };

//    private float dx, dy;


/*    public Dot(int x, int y, int color) {
        this.x = x;
        this.y = y;
        this.color = color;
    }*/

//    public Dot(float left, float top, float right, float bottom) {
//        super(left, top, right, bottom);
//
//        mLeft = left;
//        mTop = top;
//        mRight = right;
//        mBottom = bottom;
//    }

    public Dot()
    {
        this.mCol = 0;
        this.mRow = 0;
        this.mBottom = 0;
        this.mRight = 0;
        this.mTop = 0;
        this.mLeft = 0;
        this.mPaint = new Paint();
        this.mPaint.setColor(0);
        this.mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        this.mPaint.setAntiAlias(true);
    }

    public Dot(int row, int col, int color) {

        this.mRow = row;
        this.mCol = col;
//        this.mColor = color;
        this.mPaint = new Paint();
        this.mPaint.setColor(color);
        this.mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        this.mPaint.setAntiAlias(true);
    }

    public int getRow() {
        return this.mRow;
    }

    public int getCol() {
        return this.mCol;
    }

    public void setRow(int row)
    {
        this.mRow = row;
    }

    public void setCol(int col)
    {
        this.mCol = col;
    }

    public boolean isInCol (int col)
    {
        if (col == this.mCol)
        {
            return true;
        }
        return false;
    }


    public int getColor() {

        return this.mPaint.getColor();
    }

    public Paint getPaint() {
        return this.mPaint;
    }

    public void setColor(int clr)
    {
        this.mPaint.setColor(clr);
//        this.mColor = clr;
    }

    public static int getRandomColor() {
        Random rand = new Random();
        int n = rand.nextInt(5);

        return colors[n];
    }
}
