package allaround.dots;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends AppCompatActivity {

//    public Boolean isTimeGame = false;


//    public final static String _SCORE2 = "allaround.dots.SCORE";

    public void menuPlayGame (View view)
    {
//        isTimeGame = true;
        Intent intent = new Intent(this, PlayGameActivity.class);
//        intent.putExtra("GameType", isTimeGame);
        startActivity(intent);
    }

    public void menuPlayTime (View view)
    {
//        isTimeGame = false;
        Intent intent = new Intent(this, PlayTimeActivity.class);
//        intent.putExtra("GameType", isTimeGame);
        startActivity(intent);
    }

    public void menuScoreboard (View view)
    {
        Intent intent = new Intent(this, ScoreboardActivity.class);
//        intent.putExtra(_SCORE2, 0);
        startActivity(intent);
    }

    public void menuSettings (View view)
    {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
