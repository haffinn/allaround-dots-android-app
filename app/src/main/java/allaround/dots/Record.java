package allaround.dots;

import java.io.Serializable;

public class Record implements Serializable {

    private String m_name;
    private int m_score;
    private int m_boardSize = 6;
    private boolean m_timeGame = false;

    Record(String name, int score, int boardSize, boolean timeGame) {
        m_name = name;
        m_score = score;
        m_boardSize = boardSize;
        m_timeGame = timeGame;
    }

    String getName() {
        return this.m_name;
    }

    int getScore() {
        return this.m_score;
    }

    int getBoardSize()
    {
        return this.m_boardSize;
    }

    boolean isTimeGame()
    {
        return this.m_timeGame;
    }

    @Override
    public String toString() {
        return m_name + " <==> " + m_score;
    }
}

