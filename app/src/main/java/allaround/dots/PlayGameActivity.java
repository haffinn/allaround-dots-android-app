package allaround.dots;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

public class PlayGameActivity extends AppCompatActivity {

    public TextView updateScore;
    public TextView updateMoves;
    BoardView m_boardView;
    final Context context = this;
    final boolean isTimeGame = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_game);

        m_boardView = (BoardView) findViewById(R.id.boardView);
        updateScore = (TextView) findViewById(R.id.myScore);
        updateMoves = (TextView) findViewById(R.id.myMoves);




        updateScore.setText("0");
        updateMoves.setText("30");

        m_boardView.setUpdateScoresHandler(new UpdateScores() {
            @Override
            public void setViews(int playCount, int score) {
                String scoreUpdater = String.valueOf(score);
                String movesUpdater = String.valueOf(playCount);
                updateScore.setText(scoreUpdater);
                updateMoves.setText(movesUpdater);
            }

            @Override
            public void endGame(int score, int boardSize) {
                saveScore(score, boardSize);
            }

            @Override
            public void startTimer() {
                // Do nothing
            }
        });


    }

    public boolean getIsTime()
    {
        return this.isTimeGame;
    }

    private void saveScore(final int score, final int boardSize) {
        final EditText name = new EditText(context);

        name.setInputType(InputType.TYPE_CLASS_TEXT);
        AlertDialog.Builder gameOver = new AlertDialog.Builder(context);

        gameOver
                .setTitle("New highscore!")
                .setMessage("Score: " + score + "\nEnter your name")
                .setCancelable(false)
                .setView(name)
                .setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String playerName = name.getText().toString();
                                Record rec = new Record(playerName, score, boardSize, false);

                                Intent intent = new Intent(context, ScoreboardActivity.class);

                                //Senda record með
                                intent.putExtra("Record", rec);

                                startActivity(intent);
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                                Intent intent = new Intent(context, MainActivity.class);
                                startActivity(intent);
                            }
                        });

        AlertDialog alertDialog = gameOver.create();
        alertDialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_play_game, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}